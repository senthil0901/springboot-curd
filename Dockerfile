FROM openjdk:8
EXPOSE 8987
ADD target/SpringBoot-CURD-0.0.1-SNAPSHOT.war spring-boot-docker-mysql.war 
ENTRYPOINT ["java","-jar","/spring-boot-docker-mysql.war"]