package com.pack.SpringBootCURD.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.pack.SpringBootCURD.model.Student;

public interface IStudentDAO extends CrudRepository<Student, Integer>{
	
	 @Query(value = "select * from Student limit :pageid,:total", nativeQuery = true)
	 public List<Student> getStudentsByPage(@Param("pageid")int pageid,@Param("total") int total);
	 
	    @Transactional
	    @Modifying
	    @Query("UPDATE Student st SET st.email=:email,st.section=:sec,st.country=:country where st.id=:sid")
	    void updateStudent(@Param("email") String email, @Param("sec") String section,@Param("country")String country,@Param("sid") int id);

}
