package com.pack.SpringBootCURD.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pack.SpringBootCURD.dao.IStudentDAO;
import com.pack.SpringBootCURD.model.Student;


@Service
public class StudentService implements IStudentService {

	@Autowired
    IStudentDAO studentDao;
	
	public void save(Student student) {
		// TODO Auto-generated method stub
		studentDao.save(student);
		
	}

	public List<Student> getAllStudents() {
		List<Student> all = (List<Student>) studentDao.findAll();	
		return all;
	}
	
	public List<Student> getStudentsByPage(int pageid, int total)
	{
		return studentDao.getStudentsByPage(pageid,total);
	}

	public long count() {
		// TODO Auto-generated method stub
		return studentDao.count();
	}

	public Student getStudentById(int id) {
		Optional<Student> optional= studentDao.findById(id);
		Student st=null;
		if (optional.isPresent()) {
			st=optional.get();
            System.out.println(optional.get());
        } else {
            System.out.printf("No employee found with id %d%n", id);
        }
		return st;
	}

	public void update(Student emp) {
		// TODO Auto-generated method stub
		 studentDao.updateStudent(emp.getEmail(),emp.getSection(),emp.getCountry(),emp.getId());
	}

	public void delete(int id) {
		// TODO Auto-generated method stub
		studentDao.deleteById(id);
	}

	public void delete() {
		// TODO Auto-generated method stub
		studentDao.deleteAll();
	}

}